package com.md.verda.service;

import com.md.verda.model.Department;
import com.md.verda.model.FileInfo;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class VerdaServiceTest {

    @Test
    public void dowloadFile() {

        IVerdaService verdaService = new VerdaService();

        FileInfo fileInfo = new FileInfo("user","password",
                Department.investment, "707",
                "sUser","sPassword","folder");

        String actual = verdaService.dowloadFile(fileInfo);
        String expexted = "Department       :investment\n" +
                "File Type        :707\n" +
                "Verda User       :user\n" +
                "Verda Pass       :password\n" +
                "Sterling User    :sUser\n" +
                "Sterling Password:sPassword";

        Assertions.assertThat(actual).isEqualTo(expexted);
    }

}