package com.md.verda.controller;

import com.md.verda.controller.VerdaController;
import com.md.verda.model.Department;
import com.md.verda.model.FileInfo;
import com.md.verda.service.VerdaService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(VerdaController.class)
@AutoConfigureMockMvc(addFilters = false)
public class VerdaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    VerdaService verdaService;

    @MockBean
    VerdaController verdaController;

    @Test
    public void testDownloadFile() throws Exception {

        System.setProperty("jasypt.encryptor.password", "borsaistanbulverda");

        final String fileDownload = "File Downloaded.";
        Mockito.when(verdaService.dowloadFile(Mockito.any())).thenReturn(fileDownload);

        mockMvc.perform(post("/verda/investment/download-file/707")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void getFileInfo() throws Exception {
        System.setProperty("jasypt.encryptor.password", "borsaistanbulverda");

        FileInfo fileInfo = new FileInfo();
        fileInfo.setDepartment(Department.investment);
        fileInfo.setFileType("707");
        fileInfo.setVerdaUser("verda");

        Mockito.when(verdaController.getFileInfo(Mockito.anyString(), Mockito.anyString())).thenReturn(fileInfo);
        FileInfo run = verdaController.getFileInfo("investment", "707");

        Assertions.assertThat(fileInfo).isNotNull();
        Assertions.assertThat(fileInfo.getDepartment()).isEqualTo(run.getDepartment());
        Assertions.assertThat(fileInfo.getFileType()).isEqualTo(run.getFileType());
        Assertions.assertThat(fileInfo.getVerdaUser()).isEqualTo(run.getVerdaUser());

    }

}
