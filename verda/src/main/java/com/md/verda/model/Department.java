package com.md.verda.model;

public enum Department {
	
	investment("investment"), treasury("treasury"), custody("custody");
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private Department(String value) {
		this.value = value;
	}
	
	
	

}
