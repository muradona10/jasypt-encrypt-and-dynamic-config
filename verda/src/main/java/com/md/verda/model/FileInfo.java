package com.md.verda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo {
	
	private String verdaUser;
    private String verdaPassword;
    private Department department;
    private String fileType;
    private String sterlingUser;
    private String sterlingPassword;
    private String sterlingFolder;

}
