package com.md.verda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.md.verda.model.Department;
import com.md.verda.model.FileInfo;
import com.md.verda.service.IVerdaService;

@RestController
@RequestMapping("/verda")
public class VerdaController {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private IVerdaService verdaService;
	
	@PostMapping("/{department}/download-file/{filetype}")
	public String downloadFile(@PathVariable("department") String department, @PathVariable("filetype") String filetype) {
		return verdaService.dowloadFile(getFileInfo(department, filetype));
	}
	
	public FileInfo getFileInfo(String department, String filetype) {
		
		String verdaUserProp = department.concat(".verda.user");
		String verdaPassProp = department.concat(".verda.pass");
		String sterlingUserProp = department.concat(".sterling.user");
		String sterlingPassProp = department.concat(".sterling.pass");
		String sterlingFolderProp = department.concat(".sterling.folder.").concat(filetype);
		
		String verdaUser = environment.getProperty(verdaUserProp);
		String verdaPass = environment.getProperty(verdaPassProp);
		Department dept = Department.valueOf(department);
		String sterlingUser = environment.getProperty(sterlingUserProp);
		String sterlingPass = environment.getProperty(sterlingPassProp);
		String sterlingFolder = environment.getProperty(sterlingFolderProp);
		
		return new FileInfo(verdaUser, verdaPass, dept, filetype, sterlingUser, sterlingPass, sterlingFolder);
		
	}

}
