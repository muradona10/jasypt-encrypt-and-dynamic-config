package com.md.verda.service;

import org.springframework.stereotype.Service;

import com.md.verda.model.FileInfo;

import java.util.StringJoiner;

@Service
public class VerdaService implements IVerdaService{

	@Override
	public String dowloadFile(FileInfo fileInfo) {
		StringJoiner sj = new StringJoiner("\n");
		sj.add("Department       :" + fileInfo.getDepartment().getValue())
				.add("File Type        :" + fileInfo.getFileType())
				.add("Verda User       :" + fileInfo.getVerdaUser())
				.add("Verda Pass       :" + fileInfo.getVerdaPassword())
				.add("Sterling User    :" + fileInfo.getSterlingUser())
				.add("Sterling Password:" + fileInfo.getSterlingPassword());
		return sj.toString();
	}

}
