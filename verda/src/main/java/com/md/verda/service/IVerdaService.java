package com.md.verda.service;

import org.springframework.stereotype.Service;

import com.md.verda.model.FileInfo;

@Service
public interface IVerdaService {
	
	String dowloadFile(FileInfo fileInfo);

}
