package com.md.verda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@SpringBootApplication
@EnableEncryptableProperties
public class VerdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerdaApplication.class, args);
	}

}
